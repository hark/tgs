#!/bin/bash

set -e

# Copy apt downloaded packages into new device

MOUNT_POINT=$1

rsync -av /var/cache/apt/archives/ $MOUNT_POINT/apt/cache
rsync -av /var/lib/apt/lists/ $MOUNT_POINT/apt/lists

