#!/bin/bash

# Fix permissions of persistent partititon  to Tails requirements
# From the docs:
# The root directory of the persistent volume filesystem root is created by 
# the persistence configuration assistant, owned by root:root, with permissions 0775:
#
#    group-writable so that we can grant write access to other users with ACLs;
#    world-readable for end-user's convenience;
#
# additionally, an ACL grants write access on this directory to the 
# tails-persistence-setup user, so that it can edit the persistence configuration.

# The persistence configuration assistant is run with password-less sudo as 
# the tails-persistence-setup dedicated user. It creates and updates a configuration 
# file called persistence.conf, that is owned by 
# tails-persistence-setup:tails-persistence-setup, with permissions 0600 and no ACLs. 
#It refuses to read configuration files with different permissions. 

# Same applies to live-additional-software.conf file

set -e

MOUNT_POINT=$1

setfacl -m m:rwx $MOUNT_POINT
setfacl -m g::rwx $MOUNT_POINT
setfacl -m u:tails-persistence-setup:rwx $MOUNT_POINT

chown root:root $MOUNT_POINT
chmod 775 $MOUNT_POINT

cd $MOUNT_POINT
chmod 600 persistence.conf
chmod 600 live-additional-software.conf
chown tails-persistence-setup:tails-persistence-setup persistence.conf
chown tails-persistence-setup:tails-persistence-setup \
live-additional-software.conf
chown amnesia:amnesia Persistent
chmod 700 Persistent
chmod 700 lost+found
chmod 700 gnupg
chmod 700 dotfiles

ls -hal
cd
umount $MOUNT_POINT
