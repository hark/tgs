#!/usr/bin/python

import os
import random
import string

class User(object):
    """ Model storing user data
    """
    
    def __init__(self, name, account, passphrase):
        self.name = name
        self.account= account
        self.passphrase = passphrase
        self.gpg_key = None
        self.keyid = None
        self.pub_key_armored = None
        self.fingerprint = None
        self.path_home = "/home/amnesia/"+name
        self.icedove_folder_name = self.create_random_id()+".default"
        print "user created"
        
 
    

        
    def create_random_id(self):
        """Create a suitable ID for Icedove profile.
        """
        self.size = 8 
        self.chars = string.ascii_lowercase + string.digits     
        return ''.join(random.choice(self.chars) for _ in range(self.size))   