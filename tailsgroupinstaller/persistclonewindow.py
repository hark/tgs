'''
Created on 16/06/2015

@author: noid
'''
import tailsgroupinstaller
import os
from gi.repository import Gtk
from time import sleep
import logging
from tailsgroupinstaller.goodbyewindow import GoodbyeWindow

class PersistCloneWindow(object):
    '''
    classdocs
    '''


    def __init__(self, groupinstaller):
        '''
        Constructor
        '''
        self.groupinstaller = groupinstaller
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(tailsgroupinstaller.GLADE_DIR, "persistclone.glade"))
        builder.connect_signals(self)
        self.window = builder.get_object("persistclone")
        self.window.move(500,100)
        self.window.btn_clone = builder.get_object("btn_clone")
        self.window.btn_persistence = builder.get_object("btn_persistence")
        self.window.btn_configure_accounts = builder.get_object("btn_configure_accounts")

        
        self.window.clonebox = builder.get_object("clonebox")
        self.window.persistencebox = builder.get_object("persistencebox")
        self.window.configureaccountsbox = builder.get_object("configureaccountsbox")
        self.window.profiles_created_ok_dialog = builder.get_object("profiles_created_ok_dialog")
        self.afterclone_dialog = builder.get_object("afterclone_dialog")
        self.afterpersistence_dialog = builder.get_object("afterpersistence_dialog")

        self.window.user_label = builder.get_object("user_label")
        self.window.creatingusb_label = builder.get_object("creatingusb_label")
        
        self.last_user = False
        self.nusers = self.groupinstaller.nusers
    

        self.window.persistencebox.set_sensitive(False)
        self.window.configureaccountsbox.set_sensitive(False)

        
        self.actual_user = 0
        self.load_user()

        
        
    def delete_event(self, *args):
        Gtk.main_quit(*args)
    
    
    def load_user(self):

        self.window.creatingusb_label.set_label("Creating device "+str((self.actual_user+1))+" of "
                                                + str(int(self.nusers)))
        self.user = self.groupinstaller._users[self.actual_user]
        self.window.user_label.set_label("User: "+self.user.name)
        self.window.clonebox.set_sensitive(True)
        self.window.btn_clone.set_sensitive(True)
        self.window.persistencebox.set_sensitive(False)
        self.window.configureaccountsbox.set_sensitive(False)
        
    


    def btn_clone_clicked(self,widget):
        #self.afterclone_dialog.show()
        self.window.btn_clone.set_sensitive(False)

        self.groupinstaller.clonetools.clone_tails()
 
        self.window.persistencebox.set_sensitive(True)
        self.window.btn_persistence.set_sensitive(True)
    
            
    def btn_persistence_clicked(self,widget):
        #self.afterpersistence_dialog.show()
        self.window.btn_persistence.set_sensitive(False)

        self.groupinstaller.persistencetools.create_partition()
        
        self.window.configureaccountsbox.set_sensitive(True)
        self.window.btn_configure_accounts.set_sensitive(True)

    def btn_configure_accounts_clicked(self,widget):
        self.actual_user +=1
        self.window.btn_configure_accounts.set_sensitive(False)
        self.groupinstaller.persistencetools.configure_partition(self.user)
        if self.actual_user != self.groupinstaller.nusers:
            self.load_user()
        else: 
     
            logging.debug("Users completed")
            self.groupinstaller.goodbyewindow = GoodbyeWindow(self.groupinstaller)
            self.groupinstaller.persistclonewindow.window.hide()   
            self.groupinstaller.goodbyewindow.window.show()
    

        pass
    
        
    def profiles_created_ok_dialog_btn_clicked(self,widget):
        self.window.profiles_created_ok_dialog.hide()
                                               