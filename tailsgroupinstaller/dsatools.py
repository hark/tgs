'''
Created on 22/06/2015

@author: noid
'''

class DsaTools(object):
    '''
    Tools for create DSA Keys related to Pidgin OTR 
    '''


    def __init__(self):
        '''
        Constructor
        '''
    
    def gen_param(self, user):
        ''' 
        openssl genpkey -genparam -algorithm DSA out tfctestdsap.pem -pkeyopt dsa_paramgen_bits: 1024
        '''
        pass
    
    def gen_key_text(self, user, params):
        '''
        Generates key with params created with gen_param
        openssl genpkey -paramfile tfctestdsap.pem out tfctestdsak.pem -text
        '''
        pass
    
    def get_fingerprint(self,user):
        '''
        ssh-keygen -l -f testsshkeygen.pub
        '''
        pass
    