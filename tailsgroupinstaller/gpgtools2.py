'''
Created on 15/06/2015

@author: noid
'''
import tailsgroupinstaller
from tailsgroupinstaller import user
import subprocess
import os

class GpgTools2(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
    
    
    def create_batch_file(self,user):
        self.user = user
        sti ="Key-Type: RSA\n"
        sti = sti + "Key-Length:4096\n"
        #sti = sti + "Key-Length: "+str(bits)+"\n"
        sti = sti + "Passphrase: "+self.user.password+"\n"
        sti = sti + "Expire-Date: 0\n"
        sti = sti + "Subkey-Length: 4096\n"
        sti = sti + "Subkey-Type: RSA\n"
        sti = sti + "Name-Real: "+self.user.name+"\n"
        sti = sti + "Name-Email: "+self.user.account+"\n"
        sti = sti + "%pubring "+self.user.name+"_pubring\n"
        sti = sti + "%secring "+self.user.name+"_secring\n"
        sti = sti + "%commit\n"
        sti = sti + "%save\n"
        sti = sti + "%echo done\n"
        sti = sti + "\031\n\032\n"
        batch=open(user.name,"w")
        batch.write(sti)
        batch.close()
        pass
    
    def create_gpg_keys(self,user):
        self.user = user
        self.create_batch_file(user)
        path = "/home/noid/workspace/tails-group-installer/"
        batch = path+self.user.name
        print batch
        cmd = [
                "gpg --homedir ",
                path,
                "--batch", 
                "--gen-key",
                batch
                ]
        print cmd
        
        p = subprocess.Popen(cmd,shell=True,stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        
  
    
    def create_trustdb(self):
        pass
    
    def create_pubring(self):
        pass    
    
    def create_gpg_group_pubring(self, users):
        pass
     
     
    def create_gpg_subprocess(self, user):
        self.user = user 
        cmd = "gpg --batch --gen-key --yes"
        sti ="Key-Type: RSA\n"
        sti = sti + "Key-Length:4096\n"
        #sti = sti + "Key-Length: "+str(bits)+"\n"
        sti = sti + "Passphrase: "+self.user.password+"\n"
        sti = sti + "Expire-Date: 0\n"
        sti = sti + "Subkey-Length: 4096\n"
        sti = sti + "Subkey-Type: RSA\n"
        sti = sti + "Name-Real: "+self.user.name+"\n"
        sti = sti + "Name-Email: "+self.user.account+"\n"
        sti = sti + "%commit\n"
        sti = sti + "%save\n"
        sti = sti + "%echo done\n"
        sti = sti + "\031\n\032\n"

        p = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
        p.stdin.write(sti)
        p.stdin.close() #Whi pgp don't create a key in debian?
        return p.wait()    
    