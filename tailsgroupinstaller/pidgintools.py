'''
Created on 15/06/2015

@author: noid
'''

class PidginTools(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        pass
    
    def create_pidgin_folder(self,user):
        ''' Creates profile folder for pidgin
        '''
        self.create_otr_private(user)
        self.create_accounts_xml(user)
        self.create_otr_fingerprints(user)
    
    def create_otr_fingerprints(self,user):
        ''' Creates file with fingerprints of other users
        '''
        pass

    
    def create_otr_private(self,user):
        '''Create private profile for user
        '''
        pass
        
    def create_accounts_xml(self,user):
        ''' Create account file for user
        '''
        pass
    
    