'''
Created on 15/06/2015

@author: noid
'''
import tailsgroupinstaller
from tailsgroupinstaller import user
import gnupg
import threading
import shutil
import os
import subprocess
import logging

class GpgTools(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        pass

    
    
    def create_gpg_batch(self,user):
        gpg = gnupg.GPG(gnupghome=user.path_home+"/.gnupg")
        user_data = {'name_real' : user.name,
                      'name_email' : user.account,
                      'expire_date' : 0,
                      'key_type' : 'RSA',
                      'key_length' : 4096,
                      'subkey_type' : 'RSA',
                      'subkey_length' : 4096,
                      'subkey_usage' : 'encrypt,sign,auth',
                      'passphrase' : user.passphrase    
                      }
        batch_data = gpg.gen_key_input(**user_data)
        return batch_data
    
    def create_gpg_keys(self,user):
        """ Create gpg_keys openning a thread in order to not block applicacion.
        """
        self.user = user
        self.create_gpg_profile(user)

    
    def create_gpg_group_pubring(self, users):
        """
        Modify users gpg pubring and trust keys
        """
        # Import pubkeys to pubring already existent in Tails
        gpg = gnupg.GPG(gnupghome='/home/amnesia/.gnupg')
        for user in users:
            gpg.import_keys(user.pub_key_armored)


        logging.debug("Group pubring created")

    
    def create_gpg_profile(self, user):
        """
        Create gpg keyring for user
        """
        #self.groupinstaller.userwindow.window.infoprofiles_label.set_label("Testing label")
        self.user = user
        gpg = gnupg.GPG(gnupghome=user.path_home+"/gnupg", verbose=True)
        logging.debug('Imported gnupg')
        batch_data = self.create_gpg_batch(user)
        logging.debug('Created batch data')
        user.gpg_key = gpg.gen_key(batch_data)
        user.keyid = gpg.list_keys()[0]['keyid']
        logging.debug("User keyid: "+user.keyid)
        user.pub_key_armored = gpg.export_keys(user.keyid)
        user.fingerprint = gpg.list_keys()[0]['fingerprint']
        logging.debug(user.fingerprint)
        logging.debug(user.name+" gpg profile created")
        
    def create_trustdb(self,users):
        with open('/home/amnesia/.gnupg/ownertrust.txt', 'w') as f:
            for user in users:
                f.write(user.fingerprint+":6:\n")
                
        os.remove("/home/amnesia/.gnupg/trustdb.gpg")       
        proc = subprocess.Popen(
        [
                "gpg", "--import-ownertrust",
                 "/home/amnesia/.gnupg/ownertrust.txt"
    
        ]
                                )
 
    