'''
Created on 15/06/2015

@author: noid
'''
import subprocess
import os,shutil
import zipfile
import os
import config
import logging


class PersistenceTools(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.mount_point = "/home/amnesia/device_"
    

    def create_partition(self):
        
        proc = subprocess.Popen(
            [
                "/usr/bin/tails-persistence-setup","--force",
                "--override-boot-device","/org/freedesktop/UDisks/devices/sdb",
                "--override-system-partition","/org/freedesktop/UDisks/devices/sdb1",
                "--step","bootstrap"
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
            )
        
        out, err = proc.communicate()     
        logging.debug("out: "+out)
        logging.debug("err: "+err)
        logging.info("Created partition")
 
 
    def configure_partition(self,user):
        self.user = user
        logging.info("Configuring partition for user: "+self.user.name)
        self.mount_point = self.mount_point + self.user.name
        os.mkdir(self.mount_point)
        logging.debug("Created mount_point")
        self.mount_partition()
        logging.debug("Mounted partition")
        self.make_partition_writeable()
        logging.debug("Partition writeable")
        self.copy_files(self.user)
        logging.debug("Files copied")
        self.fix_permissions()
        logging.debug("Permissions fixed")

    

    def mount_partition(self):
 
        proc = subprocess.Popen(
            [
                "sudo", "-S",
                 "/bin/mount","/dev/dm-1",
                self.mount_point
            ],
            stdin=subprocess.PIPE,

                                )
        proc.communicate(input=config.ROOT_PASSWORD + "\n")

    def make_partition_writeable(self):
 
        proc = subprocess.Popen(
            [
                "sudo", "-S",
                 "/bin/chown","amnesia:amnesia",
                self.mount_point
            ],
            stdin=subprocess.PIPE,
                                )
        
        proc.communicate(input=config.ROOT_PASSWORD + "\n")
        
    def copy_files(self,user):
        #Substitue user's pubring by group's pubring
        shutil.copy2("/home/amnesia/.gnupg/pubring.gpg", user.path_home+"/gnupg/pubring.gpg")
        # copy user gnupg folder
        shutil.copytree(self.user.path_home+"/gnupg", self.mount_point+"/gnupg")
        # copy dotfiles folder which includes icedove profile
        shutil.copytree(self.user.path_home+"/dotfiles", self.mount_point+"/dotfiles")
        # create treee with defaults folders
        shutil.copytree("defaults/apt",self.mount_point+"/apt")
        shutil.copytree("defaults/Persistent",self.mount_point+"/Persistent")
        shutil.copy2("defaults/persistence.conf",self.mount_point)
        shutil.copy2("defaults/live-additional-software.conf",self.mount_point)
        
        proc =subprocess.Popen(
            [
                "sudo", "-S",
                "./rsync_apt.sh", self.mount_point
            ],
            stdin=subprocess.PIPE,
                                )
             
        proc.communicate(input=config.ROOT_PASSWORD + "\n")
        
    
    def fix_permissions(self):
       
        proc =subprocess.Popen(
            [
                "sudo", "-S",
                "./fix_permissions.sh", self.mount_point
            ],
            stdin=subprocess.PIPE,
                                )
             
        proc.communicate(input=config.ROOT_PASSWORD + "\n")
        
