'''
Created on 15/06/2015

@author: noid
'''

import threading
import gnupg
import tailsgroupinstaller


class GpgThread(threading.Thread):
    '''
    classdocs
    '''
    
    def run(self, groupinstaller, user):
        self.groupinstaller = groupinstaller
        self.user = user
        gpg = gnupg.GPG(gnupghome=user.user_alias)
        user_data = {'name_real' : user.user_alias,
                     'name_email' : user.account,
                     'expire_date' : 0,
                     'key_type' : 'RSA',
                     'key_length' : 4096,
                     'subkey_type' : 'RSA',
                     'subkey_length' : 4096,
                     'subkey_usage' : 'encrypt,sign,auth',
                     'passphrase' : user.password    
                    }
        batch_data = gpg.gen_key_input(**user_data) 
        user.gpg_key = gpg.gen_key(batch_data)
        user.keyid = gpg.list_keys()[0]['keyid']
        user.pub_key_armored = gpg.export_keys(user.keyid)   
        '''
        Constructor
        '''
  
  
      terminate = False
    twitter_username = None
    twitter_text = ''
 
    def set_buffer(self, buffer):
      self.buffer = buffer
 
    def run(self):
       while(not self.terminate):
         api = twitter.Api()
         self.twitter_text = ''
         if self.twitter_username != None:
           try:
             for status in api.GetUserTimeline(self.twitter_username):
               self.twitter_text += status.text + "\n"
           except:
             pass
           Gdk.threads_enter()
           self.buffer.set_text(self.twitter_text)
           Gdk.threads_leave()
         time.sleep(5)      