'''
Created on 15/06/2015

@author: noid
'''

import subprocess
import logging

class CloneTools(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        pass
    
    def clone_tails(self):
        proc = subprocess.Popen(
            [
                "liveusb-creator", "-u", "-n",
                "--clone", "-P", "-m", "-x"
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
            )
        logging.debug("subprocess clone open")
        out, err = proc.communicate()     
        logging.debug("out: "+out)
        logging.debug("err: "+err)   
           
        
    