'''
Created on 22/06/2015

@author: noid
'''

import tailsgroupinstaller
import os
from gi.repository import Gtk



class GoodbyeWindow(object):
    '''
    Window to say goodbye to users and close application
    '''


    def __init__(self, groupinstaller):
        '''
        Constructor
        '''
        self.groupinstaller = groupinstaller
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(tailsgroupinstaller.GLADE_DIR, "goodbye.glade"))
        builder.connect_signals(self)
        self.window = builder.get_object("goodbye")
        self.window.move(500,100)
        self.window.resize(500,600)
    
    def delete_event(self, *args):
        Gtk.main_quit(*args)