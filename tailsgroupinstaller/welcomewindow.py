'''
Created on 09/06/2015

@author: noid
'''
import tailsgroupinstaller
import os
from gi.repository import Gtk
from tailsgroupinstaller.userwindow import UserWindow
import config


class WelcomeWindow(object):
    '''
    Welcome and requirements
    '''


    def __init__(self, groupinstaller):
        '''
        Constructor
        '''
        self.groupinstaller = groupinstaller
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(tailsgroupinstaller.GLADE_DIR, "welcome.glade"))
        builder.connect_signals(self)
        self.window = builder.get_object("welcome")
        self.window.move(500,100)
        self.window.resize(500,500)
        self.window.btn_nusers = builder.get_object("btn_nusers")
        self.window.root_password_entry = builder.get_object("root_password_entry")
        
    def delete_event(self, *args):
        Gtk.main_quit(*args)
    
    def btn_start_clicked(self,*args):
        
        config.ROOT_PASSWORD = self.window.root_password_entry.get_text()
        self.groupinstaller.nusers = self.window.btn_nusers.get_value()
        self.groupinstaller.userwindow = UserWindow(self.groupinstaller)
        print "created UserWindow"
        print config.ROOT_PASSWORD
        self.groupinstaller.welcomewindow.window.hide()
        self.groupinstaller.userwindow.window.show()

    
    
