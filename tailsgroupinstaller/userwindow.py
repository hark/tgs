'''
Created on 09/06/2015

@author: noid
'''
from gi.repository import Gtk
import logging, os
import tailsgroupinstaller
import time
from threading import Thread
from tailsgroupinstaller.persistclonewindow import PersistCloneWindow
import config

class UserWindow(object):
    '''
    classdocs
    '''


    def __init__(self, groupinstaller):
        '''
        Constructor
        '''
        self.groupinstaller = groupinstaller
        builder = Gtk.Builder()
        builder.add_from_file(os.path.join(tailsgroupinstaller.GLADE_DIR, "user.glade"))
        builder.connect_signals(self)
        self.window = builder.get_object("main_window")
        self.window.move(500,100)
        self.window.alias_entry = builder.get_object("alias_entry")
        self.window.account_entry = builder.get_object("account_entry")
        self.window.passphrase_entry = builder.get_object("passphrase_entry")
        self.window.passphrasecheck_entry = builder.get_object("passphrasecheck_entry")
        self.nusers = self.groupinstaller.nusers
        self.window.actualuser_label = builder.get_object("actualuser_label")
        self.window.creatingprofiles_label = builder.get_object("creatingprofiles_label")
        self.window.actualuser_label.set_label("Creating user 1 of " + str(int(self.nusers)))
        self.actualuser = 1
        self.window.creating_users_box = builder.get_object("creating_users_box")
        self.window.creating_users_box.show()
        self.window.creating_profiles_box = builder.get_object("creating_profiles_box")
        self.window.creating_profiles_box.hide()
        self.window.info_users_box = builder.get_object("info_users_box")
        self.window.info_users_box.hide()
        self.window.create_user_btn = builder.get_object("create_user_btn")
        self.window.create_user_btn.set_sensitive(False)
        
        self.window.passphrase_dont_match_box = builder.get_object("passphrase_dont_match_box")
        self.window.passphrase_too_short_box = builder.get_object("passphrase_too_short_box")
        
        self.window.warning_account = builder.get_object("warning_account")
        self.window.warning_name = builder.get_object("warning_name")


    def passphrase_entry_changed(self,*args):
        self.passphrase = self.window.passphrase_entry.get_text()
        self.passphrase_confirmed = False
        
        if self.window.passphrase_entry.get_text() == self.window.passphrasecheck_entry.get_text():
            self.window.passphrase_dont_match_box.hide()
            self.passphrase_confirmed = True
            
        
        else:
            self.window.passphrase_dont_match_box.show()
            self.passphrase_confirmed = False
        
        if  self.check_passphrase_length(self.passphrase):
            self.window.passphrase_too_short_box.hide()
            
        else:
            self.window.passphrase_too_short_box.show()
        
        if self.passphrase_confirmed and self.check_passphrase_length(self.passphrase):
            self.window.create_user_btn.set_sensitive(True)
            
            
    def delete_event(self, *args):
        Gtk.main_quit(*args)
    
    def create_user_btn_clicked(self,*args):
        
        self.name = self.window.alias_entry.get_text()
        self.account = self.window.account_entry.get_text()
        self.passphrase = self.window.passphrase_entry.get_text()

        # Checks
        if self.check_mail_correct(self.account) and  self.check_name(self.name):
            self.create_user_checked(self.name, self.account, self.passphrase)
 
    
    
    def create_user_checked(self, name, account, passphrase):
        
        self.newuser= self.groupinstaller.create_user(
                                        name = self.name, 
                                        account = self.account, 
                                        passphrase = self.passphrase
                                        )

        #t = Thread(target=self.groupinstaller.create_gpg_profile(self.groupinstaller._users[(self.actualuser -1)]))
        #t.start()
   
        
        self.actualuser += 1 
        print str(self.actualuser)
        if self.actualuser == (self.nusers + 1):
            self.window.info_users_box.show()
            self.window.creating_profiles_box.show()
            self.window.creating_users_box.hide()
            self.window.creatingprofiles_label.set_label("About to create: "+str(int(self.nusers))+" users")

        else:
            self.next_user()
            
     
    def next_user(self):
        self.window.account_entry.set_text("")
        self.window.alias_entry.set_text("") 
        self.window.passphrase_entry.set_text("")
        self.window.passphrasecheck_entry.set_text("")
        self.window.actualuser_label.set_label("Creating user " + str(self.actualuser) + " of " + str(int(self.nusers)))
        self.window.create_user_btn.set_sensitive(False)
        self.window.passphrase_dont_match_box.show()
        self.window.passphrase_too_short_box.show()
        self.window.alias_entry.grab_focus()
        self.window.create_user_btn.set_sensitive(False)

        
    
    def go_create_profiles_btn_clicked_cb(self,*args):
        self.groupinstaller.create_gpg_keys()
        self.groupinstaller.gpgtools.create_gpg_group_pubring(self.groupinstaller._users)
        self.groupinstaller.gpgtools.create_trustdb(self.groupinstaller._users)
        self.groupinstaller.icedovetools.create_icedove_folders(self.groupinstaller._users)
        #self.groupinstaller.pidgintools.create_profiles(self.groupinstaller._users)
        
        # Change window
        self.groupinstaller.persistclonewindow = PersistCloneWindow(self.groupinstaller)
        self.groupinstaller.userwindow.window.hide()         
        self.groupinstaller.persistclonewindow.window.show()
        self.groupinstaller.persistclonewindow.window.profiles_created_ok_dialog.show()
        
        
    def check_mail_correct(self,account):
        self.account = account
        self.valid_account = True
        
        if not "@" in account:
            self.valid_account = False
        
        if self.valid_account: 
            if self.account.split("@")[1] != "riseup.net":
                self.valid_account = False
        
        if not self.valid_account:
            self.window.warning_account.show()
        else:
            return True
        
    def check_passphrase_length(self,passphrase):
        if len(passphrase) < config.PASSPHRASE_LENGTH_REQUIRED:
            print "passphrase too short"
            return False
            #self.window.warning_passphrase_too_short.show()
        else:
            return True
    
        
    def check_name(self, name):
        
        if name in (user.name for user in self.groupinstaller._users):
            self.window.warning_name.show()

            print "Name: "+name+" already exists"
            return False
        else:
            return True
    
    def warning_account_btn(self,widget) :
        self.window.warning_account.hide() 
        self.window.account_entry.grab_focus()
        
    def warning_name_btn(self,widget):
        self.window.warning_name.hide()
        self.window.alias_entry.grab_focus()
     