'''
Created on 15/06/2015

@author: noid
'''

import string
import random
import os
import shutil
import fileinput
import re
import logging

class IcedoveTools(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.icedove_profile = "defaults/icedove_profile"
        pass
    
    def create_icedove_folders(self, _users):
        for user in _users:
            self.create_icedove_folder(user)
        
    
    def create_icedove_folder(self,user):
        self.user = user
        logging.info("Start creation of icedove folder for user: "+self.user.name)
        self.create_profile_folder(user)
        logging.info("profile folder created")
        self.create_ini_file(user)
        logging.info("ini file created")
        self.modify_prefs(user)
        logging.info("prefs modified")

    def create_profile_folder(self,user):
        shutil.copytree(self.icedove_profile, user.path_home + "/dotfiles/.icedove/"
                         + user.icedove_folder_name)
      
    
    def create_ini_file(self,user):
        f = open(user.path_home +  "/dotfiles/.icedove/profiles.ini","w")
        t = """[General]
StartWithLastProfile=1
        
[Profile0]
Name=default
IsRelative=1
Path="""+user.icedove_folder_name+"\n"
            
        f.write(t)
        f.close()
        
    
    def modify_prefs2(self,user):
        self.replacements = {"riseup_mail_account":user.account.split("@")[0], 
                             "icedove_folder_name":user.icedove_folder_name
                             }
        
        prefs = user.path_home + "/dotfiles/.icedove/" + user.icedove_folder_name + "/prefs.js"
        
        lines = []
        
        with open(prefs) as infile:
            for line in infile:
                for src, target in self.replacements.iteritems():
                    line = line.replace(src,target)
                lines.append(line)
        
        with open(prefs,"w") as outfile:
            for line in lines:
                outfile.write(line)    
                    
        
    def modify_prefs(self,user):
        
        prefs = user.path_home + "/dotfiles/.icedove/" + user.icedove_folder_name + "/prefs.js"   
         
        for line in fileinput.input(prefs,1):
            print line.replace("riseup_mail_account",user.account.split("@")[0]),
        
        for line in fileinput.input(prefs,1):
            print line.replace("icedove_folder_name",user.icedove_folder_name)
        
    
