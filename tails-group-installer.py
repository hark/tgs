#!/usr/bin/env python
'''
Created on 01/06/2015

@author: noid
'''

import sys, os, subprocess 
import gnupg
import threading
import logging

from gi.repository import Gtk, Gdk,GLib, GObject

from tailsgroupinstaller import __appname__
from tailsgroupinstaller import GLADE_DIR
from tailsgroupinstaller.user import User
from tailsgroupinstaller.welcomewindow import WelcomeWindow
from tailsgroupinstaller.userwindow import UserWindow
from tailsgroupinstaller import gpgtools
from tailsgroupinstaller import gpgtools2
from tailsgroupinstaller import icedovetools
from tailsgroupinstaller import persistencetools
from tailsgroupinstaller import clonetools
from tailsgroupinstaller.goodbyewindow import GoodbyeWindow

import tailsgroupinstaller


class TailsGroupApp():
    """ Tails Group Installer Controller
    """
    
    app_name = __appname__
    glade_dir = GLADE_DIR
    root_password = "1234"
    
    def __init__(self, *args, **kwargs):
        
        # Import tools
        self.gpgtools = tailsgroupinstaller.gpgtools.GpgTools()
        self.persistencetools = tailsgroupinstaller.persistencetools.PersistenceTools()
        self.icedovetools = tailsgroupinstaller.icedovetools.IcedoveTools()
        #self.pidgintools = tailsgroupinstaller.pidgintools.PidginTools()
        self.persistencetools = tailsgroupinstaller.persistencetools.PersistenceTools()
        self.clonetools = tailsgroupinstaller.clonetools.CloneTools()
     
        
        # Define atributes
        self.user = None
        self._users = []
        self.nusers = 0
        
        #self._loaded_windows = []
        self.welcomewindow = WelcomeWindow(self)

        self.welcomewindow.window.show()
        
        logging.basicConfig(filename='tgi-debug.log',level=logging.DEBUG)

        
    def cancel_warning(self):
        """ Method to warn about user is next to quit application
        """
        pass
    
    
    def create_user(self, name, account, passphrase):
        """ Method to create a user in the app
        """
      
        self.user = User(name, account, passphrase)
        self._users.append(self.user)
        self.a = self._users[-1].name
        logging.info("Created user: " + self.a)
        #with open('./test.txt', 'w') as f:
        #self.f = open('./test' + self.a,'w')
        #self.f.write('Created user ' + self.a +'\n')
        #subprocess.check_call(['ls','-l'])
        return self.user


    def setup_pidgin(self):
        """Method to configure pidgin for the group
        Requires pidgin keys created
        """
        
        self.pidgintools.create_otr_trust(_users)
        
        pass
    def create_gpg_keys(self):
        self.actualuser = 1
        for user in self._users:
            self.gpgtools.create_gpg_profile(user)
            self.userwindow.window.creatingprofiles_label.set_label(
                "Creating profile " + str(self.actualuser) + " of " + str(int(self.nusers))
                )
            logging.info(user.name+" key created")
            self.actualuser +=1
        
        logging.info("All users keys created")
    

    
    def setup_icedove(self):
        """Method to configure icedove for the group
        Requieres gpg keys created
        """
        
        users = self._users
        self.gpgtools.create_gpg_group_pubring(users)
        self.gpgtools.create_trustdb
        self.icedovetools.create_folder(users)
    
    def clone_tails(self):
        """ Launch and manage tails-installer
        """
        
        pass
    
    def create_persistence(self, user):
        """ Launch tails-persistence-setup
        """

        pass
    
    

if __name__ == '__main__':
    app = TailsGroupApp()
    GLib.threads_init()
    Gdk.threads_init()
    Gdk.threads_enter()
    Gtk.main()
    Gdk.threads_leave()

