# Mozilla User Preferences

/* Do not edit this file.
 *
 * If you make changes to this file while the application is running,
 * the changes will be overwritten when the application exits.
 *
 * To make a manual change to preferences, you can visit the URL about:config
 */

user_pref("app.update.lastUpdateTime.addon-background-update-timer", 0);
user_pref("app.update.lastUpdateTime.blocklist-background-update-timer", 0);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1434649032);
user_pref("browser.cache.disk.capacity", 358400);
user_pref("browser.cache.disk.smart_size.first_run", false);
user_pref("browser.cache.disk.smart_size.use_old_max", false);
user_pref("browser.cache.disk.smart_size_cached_value", 215040);
user_pref("extensions.adblockplus.currentVersion", "2.1");
user_pref("extensions.blocklist.pingCountVersion", 0);
user_pref("extensions.bootstrappedAddons", "{\"{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}\":{\"version\":\"2.1\",\"type\":\"extension\",\"descriptor\":\"/usr/share/mozilla/extensions/{3550f703-e582-4d05-9a08-453d09bdfdc6}/{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}\"}}");
user_pref("extensions.databaseSchema", 16);
user_pref("extensions.enabledAddons", "%7B847b3a00-7ab1-11d4-8f02-006008948af5%7D:1.7.2");
user_pref("extensions.enigmail.configuredVersion", "1.7.2");
user_pref("extensions.getAddons.databaseSchema", 5);
user_pref("extensions.installCache", "[{\"name\":\"app-system-local\",\"addons\":{\"{847b3a00-7ab1-11d4-8f02-006008948af5}\":{\"descriptor\":\"/usr/lib/mozilla/extensions/{3550f703-e582-4d05-9a08-453d09bdfdc6}/{847b3a00-7ab1-11d4-8f02-006008948af5}\",\"mtime\":1434648667000,\"rdfTime\":1414623418000}}},{\"name\":\"app-system-share\",\"addons\":{\"{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}\":{\"descriptor\":\"/usr/share/mozilla/extensions/{3550f703-e582-4d05-9a08-453d09bdfdc6}/{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d}\",\"mtime\":1431365247000,\"rdfTime\":1373371950000}}}]");
user_pref("extensions.lastAppVersion", "31.7.0");
user_pref("extensions.lastPlatformVersion", "31.7.0");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.shownSelectionUI", true);
user_pref("mail.ab_remote_content.migrated", 1);
user_pref("mail.account.account1.identities", "id1");
user_pref("mail.account.account1.server", "server1");
user_pref("mail.account.account2.server", "server2");
user_pref("mail.account.lastKey", 2);
user_pref("mail.accountmanager.accounts", "account1,account2");
user_pref("mail.accountmanager.defaultaccount", "account1");
user_pref("mail.accountmanager.localfoldersserver", "server2");
user_pref("mail.append_preconfig_smtpservers.version", 2);
user_pref("mail.attachment.store.version", 1);
user_pref("mail.folder.views.version", 1);
user_pref("mail.identity.id1.draft_folder", 
"mailbox://riseup_mail_account@pop.riseup.net/Drafts");
user_pref("mail.identity.id1.drafts_folder_picker_mode", "0");
user_pref("mail.identity.id1.fcc_folder", 
"mailbox://riseup_mail_account@pop.riseup.net/Sent");
user_pref("mail.identity.id1.fcc_folder_picker_mode", "0");
user_pref("mail.identity.id1.fullName", "Thomas Test");
user_pref("mail.identity.id1.reply_on_top", 1);
user_pref("mail.identity.id1.smtpServer", "smtp1");
user_pref("mail.identity.id1.stationery_folder", 
"mailbox://riseup_mail_account@pop.riseup.net/Templates");
user_pref("mail.identity.id1.tmpl_folder_picker_mode", "0");
user_pref("mail.identity.id1.useremail", "riseup_mail_account@riseup.net");
user_pref("mail.identity.id1.valid", true);
user_pref("mail.openMessageBehavior.version", 1);
user_pref("mail.preferences.advanced.selectedTabIndex", 2);
user_pref("mail.rights.version", 1);
user_pref("mail.root.none", "/home/amnesia/.icedove/icedove_folder_name/Mail");
user_pref("mail.root.none-rel", "[ProfD]Mail");
user_pref("mail.root.pop3", "/home/amnesia/.icedove/icedove_folder_name/Mail");
user_pref("mail.root.pop3-rel", "[ProfD]Mail");
user_pref("mail.server.server1.check_new_mail", true);
user_pref("mail.server.server1.delete_by_age_from_server", true);
user_pref("mail.server.server1.delete_mail_left_on_server", true);
user_pref("mail.server.server1.directory", 
"/home/amnesia/.icedove/icedove_folder_name/Mail/pop.riseup.net");
user_pref("mail.server.server1.directory-rel", "[ProfD]Mail/pop.riseup.net");
user_pref("mail.server.server1.download_on_biff", true);
user_pref("mail.server.server1.hostname", "pop.riseup.net");
user_pref("mail.server.server1.leave_on_server", true);
user_pref("mail.server.server1.login_at_startup", true);
user_pref("mail.server.server1.name", "riseup_mail_account@riseup.net");
user_pref("mail.server.server1.num_days_to_leave_on_server", 14);
user_pref("mail.server.server1.port", 995);
user_pref("mail.server.server1.socketType", 3);
user_pref("mail.server.server1.storeContractID", "@mozilla.org/msgstore/berkeleystore;1");
user_pref("mail.server.server1.type", "pop3");
user_pref("mail.server.server1.userName", "riseup_mail_account");
user_pref("mail.server.server2.directory", 
"/home/amnesia/.icedove/icedove_folder_name/Mail/Local Folders");
user_pref("mail.server.server2.directory-rel", "[ProfD]Mail/Local Folders");
user_pref("mail.server.server2.hostname", "Local Folders");
user_pref("mail.server.server2.name", "Local Folders");
user_pref("mail.server.server2.storeContractID", "@mozilla.org/msgstore/berkeleystore;1");
user_pref("mail.server.server2.type", "none");
user_pref("mail.server.server2.userName", "nobody");
user_pref("mail.smtpserver.smtp1.authMethod", 3);
user_pref("mail.smtpserver.smtp1.description", "Riseup Networks Mail");
user_pref("mail.smtpserver.smtp1.hostname", "mail.riseup.net");
user_pref("mail.smtpserver.smtp1.port", 465);
user_pref("mail.smtpserver.smtp1.try_ssl", 3);
user_pref("mail.smtpserver.smtp1.username", "riseup_mail_account");
user_pref("mail.smtpservers", "smtp1");
user_pref("mail.spam.version", 1);
user_pref("mail.ui-rdf.version", 5);
user_pref("mailnews.database.global.datastore.id", "d966b14c-afca-4380-b13a-77399a006e0");
user_pref("mailnews.quotingPrefs.version", 1);
user_pref("mailnews.start_page_override.mstone", "31.7.0");
user_pref("mailnews.tags.$label1.color", "#FF0000");
user_pref("mailnews.tags.$label1.tag", "Important");
user_pref("mailnews.tags.$label2.color", "#FF9900");
user_pref("mailnews.tags.$label2.tag", "Work");
user_pref("mailnews.tags.$label3.color", "#009900");
user_pref("mailnews.tags.$label3.tag", "Personal");
user_pref("mailnews.tags.$label4.color", "#3333FF");
user_pref("mailnews.tags.$label4.tag", "To Do");
user_pref("mailnews.tags.$label5.color", "#993399");
user_pref("mailnews.tags.$label5.tag", "Later");
user_pref("mailnews.tags.version", 2);
user_pref("network.cookie.prefsMigrated", true);
user_pref("network.proxy.socks", "127.0.0.1");
user_pref("network.proxy.socks_port", 9061);
user_pref("network.proxy.type", 1);
user_pref("toolkit.telemetry.previousBuildID", "20150518224321");
user_pref("toolkit.telemetry.prompted", 2);
user_pref("toolkit.telemetry.rejected", true);
